package com.Lato;

import java.util.ArrayList;

public class Branch {

    private String name;
    private ArrayList<Customer> customers;


    public Branch(String name) {
        this.name = name;
        this.customers = new ArrayList<Customer>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private Customer findCustomer(String customerName) {
        for(int i = 0; i < customers.size(); i++) {
            Customer customer = this.customers.get(i);
            if(customer.getName().equals(customerName)) {
                return customer;
            }
        }
        return null;
    }

    public boolean newCustomer(String customerName, double initialTransaction) {
        if(findCustomer(customerName) == null) {
            this.customers.add(new Customer(customerName, initialTransaction));
            return true;
        } else {
            return false;
        }
    }

    public boolean addCustomerTransaction(String customerName, double transactionValue) {
        Customer customer = findCustomer(customerName);
        if(customer != null) {
            customer.addTransaction(transactionValue);
            return true;
        } else {
            return false;
        }

    }


}
