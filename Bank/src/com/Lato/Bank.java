package com.Lato;

import java.util.ArrayList;

public class Bank {

    private String name;
    private ArrayList<Branch> branches;

    public Bank(String name) {
        this.name = name;
        this.branches = new ArrayList<Branch>();
    }

    private Branch findBranch(String branchName) {

        for (int i = 0; i < this.branches.size(); i++) {
            Branch branch = this.branches.get(i);
            if (branch.getName().equals(branchName)) {
                return branch;
            }
        }
        return null;
    }


    public boolean addBranch(String branchName) {

        if (findBranch(branchName) == null) {
            Branch branch = new Branch(branchName);
            this.branches.add(branch);
            return true;
        }
        return false;
    }

    public boolean addCustomer(String branchName, String customerName, double initialTransaction) {
        Branch branch = findBranch(branchName);
        if (branch != null) {
            return branch.newCustomer(customerName, initialTransaction);
        } else {
            Branch newBranch = new Branch(branchName);
            this.branches.add(newBranch);
            newBranch.newCustomer(customerName, initialTransaction);
        }
        return false;
    }

    public boolean addCustomerTransaction(String branchName, String customerName, double transaction) {
        Branch branch = findBranch(branchName);
        if (branch != null) {
            return branch.addCustomerTransaction(customerName, transaction);
        } else {
            return false;
        }
    }

    public boolean listCustomers(String branchName, boolean printTransactions) {
        Branch branch = findBranch(branchName);

        if (branch != null) {

            ArrayList<Customer> customers = branch.getCustomers();

            if (printTransactions) {
                System.out.println("Customer details for branch " + branch.getName());

                for (int i = 0; i < customers.size(); i++) {
                    System.out.println("Customer: " + customers.get(i).getName() + "[" + (i + 1) + "]");

                    ArrayList<Double> transactions = customers.get(i).getTransactions();
                    System.out.println("Transactions ");
                    for (int j = 0; j < transactions.size(); j++) {
                        System.out.println("[" + (j + 1) + "]" + " Amount " + transactions.get(j));
                    }
                }
                return true;
            } else {
                System.out.println("Customer details for branch " + branch.getName());

                for (int i = 0; i < customers.size(); i++) {
                    System.out.println("Customer: " + customers.get(i).getName() + "[" + (i + 1) + "]");
                }
            return true;
                }
        } else {
            return false;
        }

}



}
