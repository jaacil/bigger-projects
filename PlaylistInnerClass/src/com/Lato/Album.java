package com.Lato;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Album {

    private String name;
    private String artist;
    //private ArrayList<Song> songs;
    private SongList songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new SongList();
    }

    public boolean addSong(String titleOfSong, double duration) {
        return this.songs.add(new Song(titleOfSong, duration));
    }

    public boolean addToPlayList(int trackNo, LinkedList<Song> playList) {
        Song song = this.songs.findSong(trackNo);
        if(song != null) {
            playList.add(song);
            return true;
        }
        System.out.println("This album does not have such a track " + trackNo);
        return false;
    }

    public boolean addToPlayList(String titleOfSong, LinkedList<Song> playList) {
        Song song = this.songs.findSong(titleOfSong);
        if(song != null) {
            playList.add(song);
            return true;
        }
        System.out.println("This song " + titleOfSong + " is not in this album");
        return false;
    }

    public static class SongList {

        private ArrayList<Song> songs;

        private SongList() {
            this.songs = new ArrayList<>();
        }

        private boolean add(Song song) {
            if(songs.contains(song)) {
                return false;
            }
            this.songs.add(song);
            return true;
        }

        private Song findSong(String titleOfSong) {

            for(Song checkSong: this.songs) { //for each Song checkSong - check if there is the same name what is in parameter
                if(checkSong.getTitle().equals(titleOfSong)) {
                    return checkSong;
                }
            }
            return null;
        }

        private Song findSong(int trackNo) {
            int index = trackNo - 1;
            if((index >= 0) && (index <= this.songs.size())) {
                return songs.get(index);
            }
            return null;
        }

    }

}
