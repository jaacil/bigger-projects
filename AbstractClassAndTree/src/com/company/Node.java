package com.company;

public class Node extends ListItem {

    public Node(Object value) {
        super(value);
    }

    @Override
    public ListItem next() {
        return this.rightLink;
    }

    @Override
    public ListItem setNext(ListItem listItem) {
        this.rightLink = listItem;
        return this.rightLink;
    }

    @Override
    public ListItem previous() {
        return this.leftLink;
    }

    @Override
    public ListItem setPrevious(ListItem listItem) {
        this.leftLink = listItem;
        return this.leftLink;
    }

    @Override
    public int compareTo(ListItem listItem) {

        if(listItem != null) {
            if(this.value.equals(listItem)) {
                return 0;
            } else {
                return (super.getValue().toString()).compareTo(listItem.getValue().toString());
            }
        } else {
            return -1;
        }
    }
}
