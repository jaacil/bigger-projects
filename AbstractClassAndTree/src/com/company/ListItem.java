package com.company;

public abstract class ListItem {

    protected ListItem rightLink;
    protected ListItem leftLink;
    protected Object value;

    public ListItem(Object value) {
        this.value = value;
    }

    public abstract ListItem next();
    public abstract ListItem setNext(ListItem listItem);
    public abstract ListItem previous();
    public abstract ListItem setPrevious(ListItem listItem);
    public abstract int compareTo(ListItem listItem);

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return this.value;
    }

}
