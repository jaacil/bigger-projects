package com.company;

public class MyLinkedList implements NodeList {

    private ListItem root;

    public MyLinkedList(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return this.root;
    }

    @Override
    public boolean addItem(ListItem listItem) {

        if(this.root == null) {
            this.root = listItem; //if list is empty - this will be head of the list
            return true;
        }

        ListItem currentItem = this.root;

        while (currentItem != null) {

            if (currentItem.compareTo(listItem) == 0) {
                System.out.println(listItem.getValue() + " is already in here");
                return false;
            } else if (currentItem.compareTo(listItem) < 0) { //new item is greater, remove right
                if(currentItem.next() != null) {
                    currentItem = currentItem.next();
                } else {
                    //end of the list - set at end
                    currentItem.setNext(listItem);
                    listItem.setPrevious(currentItem);
                    return true;
                }
            } else { //new item is less, remove left
                if(currentItem.previous() != null) {
                    currentItem.previous().setNext(listItem);
                    listItem.setPrevious(currentItem.previous());
                    listItem.setNext(currentItem);
                    currentItem.setPrevious(listItem);
                } else {
                    // begin of the list - set at first position
                    listItem.setNext(this.root);
                    this.root.setPrevious(listItem);
                    this.root = listItem;
                }
                return true;
            }
            //currentItem = currentItem.next();
        }
        return false; //?
    }

    @Override
    public boolean removeItem(ListItem listItem) {
        if(listItem != null) {
            System.out.println("Deleting item " + listItem.getValue());
        }

        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(listItem);
            if(comparison == 0) {
                //item to delete found
                if(currentItem == this.root) {
                    this.root = currentItem.next();
                } else {
                    currentItem.previous().setNext(currentItem.next());
                    if(currentItem.next() != null) {
                        currentItem.next().setPrevious(currentItem.previous());
                    }
                }
                return true;
            } else if (comparison < 0) {
                currentItem = currentItem.next();
            } else { //comparison > 0
                return false;
            }
        }
        //end of the list
        return false;
    }

    @Override
    public void traverse(ListItem root) {

        if(root == null) {
            System.out.println("The list is empty");
        } else {
            while (root != null) {
                System.out.println("Value: " + root.getValue());
                root = root.next();
            }
        }
    }
}
