package com.Lato;

public class Hamburger {

    private String name;
    private String meat;
    private double price;
    private String breadRollType;

    public Hamburger(String name, String meat, double price, String breadRollType) {
        this.name = name;
        this.meat = meat;
        this.price = price;
        this.breadRollType = breadRollType;
    }

    private String addition1Name;
    private String addition2Name;
    private String addition3Name;
    private String addition4Name;
    private double addition1Price;
    private double addition2Price;
    private double addition3Price;
    private double addition4Price;

    public void addHamburgerAddition1(String name, double price) {
        this.addition1Name = name;
        this.addition1Price = price;
    }

    public void addHamburgerAddition2(String name, double price) {
        //System.out.println("Added " + name + " for an extra " + price);
        this.addition2Name = name;
        this.addition2Price = price;
    }

    public void addHamburgerAddition3(String name, double price) {
       // System.out.println("Added " + name + " for an extra " + price);
        this.addition3Name = name;
        this.addition3Price = price;
    }

    public void addHamburgerAddition4(String name, double price) {
        //System.out.println("Added " + name + " for an extra " + price);
        this.addition4Name = name;
        this.addition4Price = price;
    }

    public double itemizeHamburger() {
        double hamburgerTotalPrice = this.price;
        System.out.println(this.name + " hamburger on a " + this.breadRollType + " with " + this.meat + ", price is " + this.price);

        if(addition1Name != null) {
            System.out.println("Added " + addition1Name + " for an extra " + addition1Price);
            hamburgerTotalPrice += addition1Price;
        }
        if(addition2Name != null) {
            System.out.println("Added " + addition2Name + " for an extra " + addition2Price);
            hamburgerTotalPrice += addition2Price;
        }
        if(addition3Name != null) {
            System.out.println("Added " + addition3Name + " for an extra " + addition3Price);
            hamburgerTotalPrice += addition3Price;
        }
        if(addition4Name != null) {
            System.out.println("Added " + addition4Name + " for an extra " + addition4Price);
            hamburgerTotalPrice += addition4Price;
        }

        return hamburgerTotalPrice;
    }

}
