package com.Lato;

public class Main {

    public static void main(String[] args) {
		Hamburger hamburger = new Hamburger("Basic", "Sasuage", 3.56, "White");
		hamburger.addHamburgerAddition1("Tomato", 0.27);
		hamburger.addHamburgerAddition2("Lettuce", 0.75);
		hamburger.addHamburgerAddition3("Cheese", 1.13);
        System.out.println("Total burger price is " + hamburger.itemizeHamburger());

        DeluxeBurger db = new DeluxeBurger();
        db.addHamburgerAddition3("meh", 50.33);
		//System.out.println("Total burger price is " + String.format("%.2f", db.itemizeHamburger()));
		System.out.println("Total burger price is " + db.itemizeHamburger());

		HealthyBurger healthyBurger = new HealthyBurger("Bacon", 5.67);
		healthyBurger.addHamburgerAddition1("Egg", 5.43);
		healthyBurger.addHealthyAddition1("Lentils", 3.41);
        System.out.println("Total burger price is " + healthyBurger.itemizeHamburger());
    }
}
