package com.Lato;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("111222333");

    public static void main(String[] args) {


        boolean quit = false;
        startPhone();
        printActions();
        while (!quit) {
            System.out.println("\nEnter action: (6 to show menu)");
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    System.out.println("\nShutting down");
                    break;
                case 1: mobilePhone.printContacts();
                break;
                case 2: addNewContact();
                break;
                case 3: updateContact();
                break;
                case 4: removeContact();
                break;
                case 5: queryContact();
                break;
                case 6: printActions();
                break;
            }
        }
    }

    private static void addNewContact() {
        System.out.print("Enter new contact name: ");
        String name = scanner.nextLine();
        System.out.print("Enter number for this contact: ");
        String number = scanner.nextLine();
        Contact newContact = Contact.createContact(name, number);
        if(mobilePhone.addNewContact(newContact)) {
            System.out.println("New contact was created; name = " + name + ", phone = " + number);
        } else {
            System.out.println("Cannot add, " + name + " is already existing!");
        }
    }

    public static void updateContact() {
        System.out.print("Enter a contact name to update: ");
        String nameToUpdate = scanner.nextLine();

        Contact existing = mobilePhone.queryContact(nameToUpdate);
        if(existing == null) {
            System.out.println("Contact not existing");
            return;
        } else {
            System.out.print("Enter new contact name: ");
            String name = scanner.nextLine();
            System.out.print("Enter new contact number: ");
            String number = scanner.nextLine();

            Contact newContact = Contact.createContact(name, number);
            if(mobilePhone.updateContact(existing, newContact)) {
                System.out.println("Successfully updated!");
            } else {
                System.out.println("Error updating record");
            }
        }
    }

    public static void removeContact() {
        System.out.print("Enter a contact name to remove: ");
        String nameToDelete = scanner.nextLine();

        Contact existing = mobilePhone.queryContact(nameToDelete);
        if(existing == null) {
            System.out.println("Contact not existing");
            return;
        } else {
            if(mobilePhone.removeContact(existing)) {
                System.out.println("Successfully deleted!");
            } else {
                System.out.println("Error deleting record");
            }
        }
    }

    public static void queryContact() {
        System.out.print("Enter a name: ");
        String name = scanner.nextLine();

        Contact existing = mobilePhone.queryContact(name);
        if(existing == null) {
            System.out.println("Contact not existing");
            return;
        }

        System.out.println("Name " + existing.getName() + ", number " + existing.getPhoneNumber());

    }

    private static void startPhone() {
        System.out.println("Starting phone...");
    }

    private static void printActions() {
        System.out.println("\nAvailable actions:\npress");
        System.out.println("0 - to shutdown:\n" +
                "1 - to print contacts\n" +
                "2 - to add a new contact\n" +
                "3 - to update a existing contact\n" +
                "4 - to remove a existing contact\n" +
                "5 - query if an existing contact exists\n" +
                "6 - to print a list of available actions.");
        System.out.println("Choose your action: ");
    }


}
